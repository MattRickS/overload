import pytest

import overload


def test_overload():
    class A(object):
        def __init__int_pair(self, a, b):
            self.value = a + b

        def __init__int(self, a):
            self.value = a

        def __init__str(self, a):
            self.value = int(a)

        def __init__na(self):
            self.value = 0

        __init__ = overload.OverloadedFunction(
            {
                (int, int): __init__int_pair,
                (int,): __init__int,
                (str,): __init__str,
                (): __init__na,
            },
            is_bound=True,
        )

        def _func_int(self, a):
            return a + 10

        def _func_int_pair(self, a, b):
            return a + b

        func = overload.OverloadedFunction(
            {
                (int,): _func_int,
                (int, int): _func_int_pair,
            },
            is_bound=True,
        )

    def _func_int_triple(a, b, c=1):
        return a + b + c

    def _func_int(a):
        return a

    func = overload.OverloadedFunction(
        {
            (int, int, int): _func_int_triple,
            (int,): _func_int,
        },
    )

    assert A(1, 2).value == 3
    assert A("1").value == 1
    assert A(5).value == 5
    assert A().value == 0
    assert A().func(1) == 11
    assert A().func(1, 1) == 2

    assert func(1) == 1
    assert func(1, 1) == 3
    assert func(1, 1, 2) == 4


def test_no_matching_overload():
    def _func_int(a):
        pass

    def _func_pair(a, b):
        pass

    func = overload.OverloadedFunction({
        (int,): _func_int,
        (int, int): _func_pair,
    })

    with pytest.raises(overload.OverloadError):
        func("value")


def test_invalid_num_args():
    def func(a):
        pass

    with pytest.raises(overload.OverloadError):
        overload.OverloadedFunction({(int, int): func})


def test_invalid_variable_args():
    def func_args(a, *args):
        pass

    with pytest.raises(overload.OverloadError):
        overload.OverloadedFunction({(int,): func_args})


def test_invalid_variable_kwargs():
    def func_kwargs(a, **kwargs):
        pass

    with pytest.raises(overload.OverloadError):
        overload.OverloadedFunction({(int,): func_kwargs})


def test_invalid_default_arg_type():
    def func(a, b=1):
        pass

    with pytest.raises(overload.OverloadError):
        overload.OverloadedFunction({(int, str): func})


def test_invalid_overload_duplicate_keywords():
    def func(a):
        pass

    def same_func(a, b=1):
        pass

    # Defaults are not sufficient to change the type as func(1) would be ambiguous
    with pytest.raises(overload.OverloadError):
        overload.OverloadedFunction({(int,): func, (int, int): same_func})
