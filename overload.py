import collections
import functools
import inspect
import sys


TypedFunction = collections.namedtuple(
    "TypedFunction", "func types args num_args num_pos_args is_bound default_types"
)


class OverloadError(Exception):
    """ All errors related to method overloading """


def build_type_func(func, types, is_bound=False):
    """
    Args:
        func (callable): Function to wrap
        types (tuple[Type, ...]): Iterable of types for the function parameters.
            Must have the same number of parameters, including keywords but
            excluding the "self" argument.
        is_bound (:obj:`bool`, optional): Whether or not the method being
            overloaded is bound to a class. This is required as methods are
            defined before the class so there is no way to retrieve the
            information from the function.

    Returns:
        TypedFunction:
    """
    if sys.version_info[0] < 3:
        arg_spec = inspect.getargspec(func)
        keywords = arg_spec.keywords
    else:
        arg_spec = inspect.getfullargspec(func)
        keywords = arg_spec.varkw

    if arg_spec.varargs is not None or keywords is not None:
        raise OverloadError("Cannot overload a function with variable args")

    arg_names = arg_spec.args[int(is_bound) :]
    if len(arg_names) != len(types):
        raise OverloadError("Invalid number of types")

    num_pos_args = len(arg_names)
    if arg_spec.defaults:
        num_pos_args -= len(arg_spec.defaults)
        default_types = {
            k: type(v)
            for k, v in zip(arg_spec.args[-len(arg_spec.defaults) :], arg_spec.defaults)
            if v is not None
        }
        mapped_types = dict(zip(arg_names, types))
        for k, v in default_types.items():
            if mapped_types[k] != v:
                raise OverloadError("Defaults do not match given types")
    else:
        default_types = {}

    # Must get arg names before the closure to avoid late binding
    func_item = TypedFunction(
        func,
        list(types),
        arg_names,
        len(arg_names),
        num_pos_args,
        is_bound,
        default_types,
    )
    return func_item


def build_type_list(typed_func, args, kwargs):
    """
    Raises:
        OverloadError: If the provided arguments cannot be built into a matching
            list of types

    Args:
        typed_func (TypedFunction):
        args (list|tuple): List of positional values passed to the func
        kwargs (dict): Dictionary of keyword arguments and values passed to the
            func

    Returns:
        list[Type]: List of types
    """
    if (len(args) + len(kwargs)) > typed_func.num_args:
        raise OverloadError("Too many arguments")

    provided_types = typed_func.default_types.copy()
    provided_types.update(dict(zip(typed_func.args, map(type, args))))
    provided_types.update({k: type(v) for k, v in kwargs.items()})

    missing = set(typed_func.args) - set(provided_types)
    if missing:
        raise OverloadError("Missing required arguments: {}".format(missing))

    types = [provided_types[arg] for arg in typed_func.args]
    return types


def conflicts(typed_func_1, typed_func_2):
    """
    Args:
        typed_func_1 (TypedFunction):
        typed_func_2 (TypedFunction):

    Returns:
        bool: Whether or not the two overloaded functions could have ambiguous
            call signatures
    """
    return typed_func_1.is_bound == typed_func_2.is_bound and (
        typed_func_1.types == typed_func_2.types
        or typed_func_1.types[: typed_func_1.num_pos_args]
        == typed_func_2.types[: typed_func_2.num_pos_args]
    )


def matches(typed_func, args, kwargs):
    """
    Args:
        typed_func (TypedFunction):
        args (list|tuple): List of positional values passed to the func
        kwargs (dict): Dictionary of keyword arguments and values passed to the
            func

    Returns:
        bool: Whether or not the provided arguments match the overloaded function
    """
    try:
        return (
            build_type_list(typed_func, args[int(typed_func.is_bound) :], kwargs)
            == typed_func.types
        )
    except OverloadError:
        return False


class OverloadedFunction(object):
    def __init__(self, overload_mapping, is_bound=False):
        """
        Args:
            overload_mapping (dict): Dictionary mapping a tuple of Types to a
                function. Must have the same number of types as parameters,
                including keywords but excluding the "self" argument.
            is_bound (:obj:`dict`, optional): Whether or not the method being
                overloaded is bound to a class. This is required as methods are
                defined before the class so there is no way to retrieve the
                information from the function.
        """
        self._cls = None
        self._typed_functions = []

        for types, func in overload_mapping.items():
            typed_func = build_type_func(func, types, is_bound=is_bound)
            for tf in self._typed_functions:
                if conflicts(typed_func, tf):
                    raise OverloadError(
                        "Conflict: {} : {}".format(typed_func.func, tf.func)
                    )
            self._typed_functions.append(typed_func)

    def __get__(self, instance, owner):
        obj = owner or instance
        if obj is not None:
            return functools.partial(self, obj)
        return self

    def __call__(self, *args, **kwargs):
        for typed_func in self._typed_functions:
            if matches(typed_func, args, kwargs):
                return typed_func.func(*args, **kwargs)
        raise OverloadError(
            "No matching function for args: {}, {}".format(args, kwargs)
        )
